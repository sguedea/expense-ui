import { Component, OnInit } from '@angular/core';
import { Expense } from 'src/app/models/expense';
import { ExpenseService } from 'src/app/services/expense.service';

@Component({
  selector: 'app-list-of-expenses',
  templateUrl: './list-of-expenses.component.html',
  styleUrls: ['./list-of-expenses.component.css']
})
export class ListOfExpensesComponent implements OnInit {

  expenses: Expense[];

  searchExpense = {
    keyword: '',
    sortBy: 'Name'
  };

  constructor(private expenseService: ExpenseService) { }

  ngOnInit(): void {
    this.displayExpenses();
  }


  displayExpenses() {
    this.expenseService.getExpenses().subscribe(
      data => this.expenses = this.searchExpenses(data)
    );
  }

  deleteExpense(id: number) {
    this.expenseService.delete(id).subscribe(
      data => this.expenses = data
    );
  }


  // filter based on the name of the expense
  searchExpenses(expenses: Expense[]) {
    return expenses.filter((e) => {
      return e.expense.toLowerCase().includes(this.searchExpense.keyword.toLowerCase());
    }).sort((a, b) => {
      if (this.searchExpense.sortBy === 'Name') {
        return a.expense.toLowerCase() < b.expense.toLowerCase() ? -1 : 1;
      }
      else if (this.searchExpense.sortBy === 'Amount') {
        return a.amount > b.amount ? -1 : 1;
      }
    });
  }

}
