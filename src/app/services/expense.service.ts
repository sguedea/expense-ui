import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Expense } from '../models/expense';

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {

  // going to make a request to this url
  private getUrl = 'http://localhost:8080/expenses';

  constructor(private httpClient: HttpClient) { }

  getExpenses(): Observable<Expense[]> {
    return this.httpClient.get<Expense[]>(this.getUrl).pipe(
      map(response => response)
    );
  }

  // create and save new expense
  saveExpense(expense: Expense): Observable<Expense> {
    return this.httpClient.post<Expense>(this.getUrl, expense);
  }

  getExpense(id: number): Observable<Expense> {
    return this.httpClient.get<Expense>(`${this.getUrl}/${id}`).pipe(
      map(response => response)
    );
  }

  delete(id: number): Observable<any> {
    return this.httpClient.delete(`${this.getUrl}/${id}`, {responseType: 'text'});
  }

}
